provider "google" {
  project = "reysa-sandbox"
  region = "asia-southeast2"
  zone = "asia-southeast2-a"
}

resource "google_compute_network" "default" {
  name = "default"
}

resource "google_compute_subnetwork" "default" {
  name          = "default"
  region        = "asia-southeast2"
  network       = google_compute_network.default.self_link
  ip_cidr_range = "10.184.0.0/20"
}
