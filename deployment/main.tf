provider "google" {
  project = "reysa-sandbox"
  region = "asia-southeast2"
  zone = "asia-southeast2-a"
}

resource "google_compute_network" "vpc_network" {
  name                    = "tf-vpc"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "default" {
  name          = "tf-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "asia-southeast2"
  network       = google_compute_network.vpc_network.id
}


# Create a single Compute Engine instance
resource "google_compute_instance" "default" {
  name         = "tf-instance-fresh"
  machine_type = "e2-small"
  zone         = "asia-southeast2-a"
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }

  # Install Flask
  metadata_startup_script = "sudo apt update"

  network_interface {
    # network = google_compute_network.default.self_link
    subnetwork = google_compute_subnetwork.default.id

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

resource "google_compute_firewall" "ssh" {
  name = "allow-ssh"
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}